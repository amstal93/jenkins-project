Started by user jenkins
Obtained Jenkinsfile from git https://gitlab.com/gbazack/jenkins-project
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/Jenkins-Project
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Checkout SCM)
[Pipeline] checkout
The recommended git tool is: git
No credentials specified
 > git rev-parse --resolve-git-dir /var/jenkins_home/workspace/Jenkins-Project/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://gitlab.com/gbazack/jenkins-project # timeout=10
Fetching upstream changes from https://gitlab.com/gbazack/jenkins-project
 > git --version # timeout=10
 > git --version # 'git version 2.30.2'
 > git fetch --tags --force --progress -- https://gitlab.com/gbazack/jenkins-project +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 9862df3d313ce90030d94eb3d86f5ab6afc8c54f (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9862df3d313ce90030d94eb3d86f5ab6afc8c54f # timeout=10
Commit message: "update: ci pipeline"
 > git rev-list --no-walk 349532c22cdc810a9436d29bb1b21e8f9cef73c7 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker inspect -f . python:3.8.10
.
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] withDockerContainer
Jenkins seems to be running inside container 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635
$ docker run -t -d -u 0:0 -w /var/jenkins_home/workspace/Jenkins-Project --volumes-from 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635 -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** python:3.8.10 cat
$ docker top fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d -eo pid,comm
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (checkout-gitlab)
[Pipeline] echo
Checking out gitlab repository
[Pipeline] checkout
The recommended git tool is: git
No credentials specified
Warning: JENKINS-30600: special launcher org.jenkinsci.plugins.docker.workflow.WithContainerStep$Decorator$1@244f6874; decorates hudson.Launcher$LocalLauncher@5d027020 will be ignored (a typical symptom is the Git executable not being run inside a designated container)
 > git rev-parse --resolve-git-dir /var/jenkins_home/workspace/Jenkins-Project/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://gitlab.com/gbazack/jenkins-project # timeout=10
Fetching upstream changes from https://gitlab.com/gbazack/jenkins-project
 > git --version # timeout=10
 > git --version # 'git version 2.30.2'
 > git fetch --tags --force --progress -- https://gitlab.com/gbazack/jenkins-project +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 9862df3d313ce90030d94eb3d86f5ab6afc8c54f (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9862df3d313ce90030d94eb3d86f5ab6afc8c54f # timeout=10
Commit message: "update: ci pipeline"
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Init-env)
[Pipeline] echo
Preparing the environment
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Security)
[Pipeline] parallel
[Pipeline] { (Branch: Bandit-test)
[Pipeline] { (Branch: Safety-check)
[Pipeline] stage
[Pipeline] { (Bandit-test)
[Pipeline] stage
[Pipeline] { (Safety-check)
[Pipeline] echo
Scanning common security issues in Python code
[Pipeline] sh
[Pipeline] echo
Check installed dependencies for known vulnerabilities
[Pipeline] sh
+ . venv/bin/activate
+ deactivate nondestructive
+ [ -n  ]
+ [ -n  ]
+ [ -n  -o -n  ]
+ [ -n  ]
+ unset VIRTUAL_ENV
+ [ ! nondestructive = nondestructive ]
+ VIRTUAL_ENV=/home/gbazack/GIT/jenkins-project/venv
+ export VIRTUAL_ENV
+ _OLD_VIRTUAL_PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ PATH=/home/gbazack/GIT/jenkins-project/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export PATH
+ [ -n  ]
+ [ -z  ]
+ _OLD_VIRTUAL_PS1=# 
+ [ x(venv)  != x ]
+ PS1=(venv) # 
+ export PS1
+ [ -n  -o -n  ]
+ . venv/bin/activate
+ deactivate nondestructive
+ [ -n  ]
+ [ -n  ]
+ [ -n  -o -n  ]
+ [ -n  ]
+ unset VIRTUAL_ENV
+ [ ! nondestructive = nondestructive ]
+ VIRTUAL_ENV=/home/gbazack/GIT/jenkins-project/venv
+ export VIRTUAL_ENV
+ _OLD_VIRTUAL_PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ PATH=/home/gbazack/GIT/jenkins-project/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export PATH
+ [ -n  ]
+ [ -z  ]
+ _OLD_VIRTUAL_PS1=# 
+ [ x(venv)  != x ]
+ PS1=(venv) # 
+ export PS1
+ [ -n  -o -n  ]
[Pipeline] sh
+ + bandit -r app/
tee -a reports/bandit.json
/var/jenkins_home/workspace/Jenkins-Project@tmp/durable-b6ef8926/script.sh: 1: /var/jenkins_home/workspace/Jenkins-Project@tmp/durable-b6ef8926/script.sh: bandit: not found
[Pipeline] sh
+ safety check --full-report --json
/var/jenkins_home/workspace/Jenkins-Project@tmp/durable-c252e1ab/script.sh: 1: /var/jenkins_home/workspace/Jenkins-Project@tmp/durable-c252e1ab/script.sh: safety: not found
+ tee -a reports/safety.json
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // parallel
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Codequality)
[Pipeline] parallel
[Pipeline] { (Branch: Hadolint)
[Pipeline] { (Branch: Flake8)
[Pipeline] { (Branch: Pylama)
[Pipeline] stage
[Pipeline] { (Hadolint)
[Pipeline] stage
[Pipeline] { (Flake8)
[Pipeline] stage
[Pipeline] { (Pylama)
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/Jenkins-Project@2
[Pipeline] {
[Pipeline] echo
Static testing with Python Flake8 package
[Pipeline] sh
[Pipeline] echo
Python code audit with pylama
+ . venv/bin/activate
+ deactivate nondestructive
+ [ -n  ]
+ [ -n  ]
+ [ -n  -o -n  ]
+ [ -n  ]
+ unset VIRTUAL_ENV
+ [ ! nondestructive = nondestructive ]
+ VIRTUAL_ENV=/home/gbazack/GIT/jenkins-project/venv
+ export VIRTUAL_ENV
+ _OLD_VIRTUAL_PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ PATH=/home/gbazack/GIT/jenkins-project/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export PATH
+ [ -n  ]
+ [ -z  ]
+ _OLD_VIRTUAL_PS1=# 
+ [ x(venv)  != x ]
+ PS1=(venv) # 
+ export PS1
+ [ -n  -o -n  ]
[Pipeline] sh
[Pipeline] checkout
The recommended git tool is: git
No credentials specified
Warning: JENKINS-30600: special launcher org.jenkinsci.plugins.docker.workflow.WithContainerStep$Decorator$1@679fca4e; decorates hudson.Launcher$LocalLauncher@445bbcec will be ignored (a typical symptom is the Git executable not being run inside a designated container)
 > git rev-parse --resolve-git-dir /var/jenkins_home/workspace/Jenkins-Project@2/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://gitlab.com/gbazack/jenkins-project # timeout=10
Fetching upstream changes from https://gitlab.com/gbazack/jenkins-project
 > git --version # timeout=10
 > git --version # 'git version 2.30.2'
 > git fetch --tags --force --progress -- https://gitlab.com/gbazack/jenkins-project +refs/heads/*:refs/remotes/origin/* # timeout=10
+ . venv/bin/activate
+ deactivate nondestructive
+ [ -n  ]
+ [ -n  ]
+ [ -n  -o -n  ]
+ [ -n  ]
+ unset VIRTUAL_ENV
+ [ ! nondestructive = nondestructive ]
+ VIRTUAL_ENV=/home/gbazack/GIT/jenkins-project/venv
+ export VIRTUAL_ENV
+ _OLD_VIRTUAL_PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ PATH=/home/gbazack/GIT/jenkins-project/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export PATH
+ [ -n  ]
+ [ -z  ]
+ _OLD_VIRTUAL_PS1=# 
+ [ x(venv)  != x ]
+ PS1=(venv) # 
+ export PS1
+ [ -n  -o -n  ]
[Pipeline] writeFile
[Pipeline] writeFile
[Pipeline] sh
+ flake8 --exit-zero --statistics app/
/var/jenkins_home/workspace/Jenkins-Project@tmp/durable-34c33efc/script.sh: 1: /var/jenkins_home/workspace/Jenkins-Project@tmp/durable-34c33efc/script.sh: flake8: not found
+ tee -a reports/flake8.json
[Pipeline] sh
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 9862df3d313ce90030d94eb3d86f5ab6afc8c54f (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9862df3d313ce90030d94eb3d86f5ab6afc8c54f # timeout=10
+ pylama app --format json
+ /var/jenkins_home/workspace/Jenkins-Project@tmp/durable-c839848a/script.sh: 1: /var/jenkins_home/workspace/Jenkins-Project@tmp/durable-c839848a/script.sh: pylama: not found
tee -a reports/pylama.json
Commit message: "update: ci pipeline"
Post stage
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] withEnv
[Pipeline] {
[Pipeline] recordIssues
[Flake8] [-ERROR-] Skipping file 'reports/flake8.json' because it's empty
[Flake8] Searching for all files in '/var/jenkins_home/workspace/Jenkins-Project' that match the pattern 'reports/flake8.json'
[Flake8] -> found 1 file
[Flake8] Skipping post processing
[Flake8] No filter has been set, publishing all 0 issues
[Flake8] Repository miner is not configured, skipping repository mining
[Flake8] Reference build recorder is not configured
[Flake8] Obtaining reference build from same job (Jenkins-Project)
[Flake8] Using reference build 'Jenkins-Project #216' to compute new, fixed, and outstanding issues
[Flake8] Issues delta (vs. reference build): outstanding: 0, new: 0, fixed: 0
[Flake8] No quality gates have been set - skipping
[Flake8] Enabling health report (Healthy=1, Unhealthy=9, Minimum Severity=HIGH)
[Flake8] Created analysis result for 0 issues (found 0 new issues, fixed 0 issues)
[Flake8] Attaching ResultAction with ID 'flake8' to build 'Jenkins-Project #218'.
[Pipeline] isUnix
[Pipeline] withEnv
[Checks API] No suitable checks publisher found.
[Pipeline] {
[Pipeline] sh
+ docker inspect -f . hadolint/hadolint:v2.0.0-alpine
[Pipeline] }
.
[Pipeline] // stage
[Pipeline] }
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] withDockerContainer
Jenkins seems to be running inside container 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker run -t -d -u 0:0 -w /var/jenkins_home/workspace/Jenkins-Project@2 --volumes-from 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635 -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** hadolint/hadolint:v2.0.0-alpine cat
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker top af933878c9cf5eda7a9e2c84f688bba5711a25394ac46496b343a8394e48dbf8 -eo pid,comm
[Pipeline] {
[Pipeline] echo
Lint the dockerfile and docker-compose files
[Pipeline] writeFile
[Pipeline] sh
+ hadolint -f json app/Dockerfile
+ tee -a reports/hadolint.json
[{"line":11,"code":"DL3013","message":"Pin versions in pip. Instead of `pip install <package>` use `pip install <package>==<version>` or `pip install --requirement <requirements file>`","column":1,"file":"app/Dockerfile","level":"warning"},{"line":11,"code":"DL3042","message":"Avoid use of cache directory with pip. Use `pip install --no-cache-dir <package>`","column":1,"file":"app/Dockerfile","level":"warning"},{"line":13,"code":"DL3013","message":"Pin versions in pip. Instead of `pip install <package>` use `pip install <package>==<version>` or `pip install --requirement <requirements file>`","column":1,"file":"app/Dockerfile","level":"warning"},{"line":18,"code":"DL3025","message":"Use arguments JSON notation for CMD and ENTRYPOINT arguments","column":1,"file":"app/Dockerfile","level":"warning"}]
Post stage
[Pipeline] recordIssues
[Dockerfile Lint] Searching for all files in '/var/jenkins_home/workspace/Jenkins-Project@2' that match the pattern 'reports/hadolint.json'
[Dockerfile Lint] -> found 1 file
[Dockerfile Lint] Successfully parsed file /var/jenkins_home/workspace/Jenkins-Project@2/reports/hadolint.json
[Dockerfile Lint] -> found 0 issues (skipped 0 duplicates)
[Dockerfile Lint] Skipping post processing
[Dockerfile Lint] No filter has been set, publishing all 0 issues
[Dockerfile Lint] Repository miner is not configured, skipping repository mining
[HadoLint] Searching for all files in '/var/jenkins_home/workspace/Jenkins-Project@2' that match the pattern 'reports/hadolint.json'
[HadoLint] -> found 1 file
[HadoLint] Successfully parsed file /var/jenkins_home/workspace/Jenkins-Project@2/reports/hadolint.json
[HadoLint] -> found 4 issues (skipped 0 duplicates)
[HadoLint] Post processing issues on 'Master' with source code encoding 'UTF-8'
[HadoLint] Creating SCM blamer to obtain author and commit information for affected files
[HadoLint] -> No blamer installed yet. You need to install the 'git-forensics' plugin to enable blaming for Git.
[HadoLint] Resolving file names for all issues in workspace '/var/jenkins_home/workspace/Jenkins-Project@2'
[HadoLint] -> resolved paths in source directory (1 found, 0 not found)
[HadoLint] Resolving module names from module definitions (build.xml, pom.xml, or Manifest.mf files)
[HadoLint] -> resolved module names for 4 issues
[HadoLint] Resolving package names (or namespaces) by parsing the affected files
[HadoLint] -> resolved package names of 1 affected files
[HadoLint] No filter has been set, publishing all 4 issues
[HadoLint] Creating fingerprints for all affected code blocks to track issues over different builds
[HadoLint] -> created fingerprints for 4 issues (skipped 0 issues)
[HadoLint] Copying affected files to Jenkins' build folder '/var/jenkins_home/jobs/Jenkins-Project/builds/218/files-with-issues'
[HadoLint] -> 1 copied, 0 not in workspace, 0 not-found, 0 with I/O error
[HadoLint] Repository miner is not configured, skipping repository mining
[Static Analysis] Reference build recorder is not configured
[Static Analysis] Obtaining reference build from same job (Jenkins-Project)
[Static Analysis] Using reference build 'Jenkins-Project #216' to compute new, fixed, and outstanding issues
[Static Analysis] Issues delta (vs. reference build): outstanding: 4, new: 0, fixed: 0
[Static Analysis] No quality gates have been set - skipping
[Static Analysis] Enabling health report (Healthy=1, Unhealthy=9, Minimum Severity=HIGH)
[Static Analysis] Created analysis result for 4 issues (found 0 new issues, fixed 0 issues)
[Static Analysis] Attaching ResultAction with ID 'analysis' to build 'Jenkins-Project #218'.
[Checks API] No suitable checks publisher found.
[Pipeline] }
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker stop --time=1 af933878c9cf5eda7a9e2c84f688bba5711a25394ac46496b343a8394e48dbf8
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker rm -f af933878c9cf5eda7a9e2c84f688bba5711a25394ac46496b343a8394e48dbf8
[Pipeline] // withDockerContainer
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // parallel
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Coverage)
[Pipeline] echo
Publishing Code coverage
[Pipeline] sh
+ . venv/bin/activate
+ deactivate nondestructive
+ [ -n  ]
+ [ -n  ]
+ [ -n  -o -n  ]
+ [ -n  ]
+ unset VIRTUAL_ENV
+ [ ! nondestructive = nondestructive ]
+ VIRTUAL_ENV=/home/gbazack/GIT/jenkins-project/venv
+ export VIRTUAL_ENV
+ _OLD_VIRTUAL_PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ PATH=/home/gbazack/GIT/jenkins-project/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
+ export PATH
+ [ -n  ]
+ [ -z  ]
+ _OLD_VIRTUAL_PS1=# 
+ [ x(venv)  != x ]
+ PS1=(venv) # 
+ export PS1
+ [ -n  -o -n  ]
[Pipeline] sh
+ python -m pip install coverage
Collecting coverage
  Downloading coverage-6.3.2-cp38-cp38-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_17_x86_64.manylinux2014_x86_64.whl (212 kB)
Installing collected packages: coverage
Successfully installed coverage-6.3.2
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
WARNING: You are using pip version 21.1.3; however, version 22.0.4 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
[Pipeline] sh
+ coverage report --no-skip-covered -m app/manage.py app/vapormap/__init__.py app/vapormap/urls.py app/vapormap/wsgi.py app/vapormap/settings/__init__.py app/vapormap/settings/base.py app/vapormap/settings/development.py app/vapormap/settings/production.py app/api/__init__.py app/api/api_test.py app/api/apps.py app/api/models.py app/api/tests.py app/api/views.py app/api/migrations/0001_initial.py app/api/migrations/0002_auto_20190524_1256.py app/api/migrations/0003_auto_20190529_1149.py app/api/migrations/__init__.py
Name                                            Stmts   Miss  Cover   Missing
-----------------------------------------------------------------------------
app/api/__init__.py                                 0      0   100%
app/api/api_test.py                                28     28     0%   1-69
app/api/apps.py                                     3      3     0%   1-5
app/api/migrations/0001_initial.py                  5      5     0%   3-13
app/api/migrations/0002_auto_20190524_1256.py       4      4     0%   3-12
app/api/migrations/0003_auto_20190529_1149.py       5      5     0%   3-13
app/api/migrations/__init__.py                      0      0   100%
app/api/models.py                                  13     13     0%   1-33
app/api/tests.py                                   11     11     0%   1-21
app/api/views.py                                   37     37     0%   2-75
app/manage.py                                       9      9     0%   2-16
app/vapormap/__init__.py                            0      0   100%
app/vapormap/settings/__init__.py                   0      0   100%
app/vapormap/settings/base.py                      12     12     0%   6-67
app/vapormap/settings/development.py                8      8     0%   5-31
app/vapormap/settings/production.py                 9      9     0%   5-30
app/vapormap/urls.py                               11     11     0%   4-27
app/vapormap/wsgi.py                                4      4     0%   6-11
-----------------------------------------------------------------------------
TOTAL                                             159    159     0%
[Pipeline] sh
+ coverage html --no-skip-covered --show-contexts --ignore-errors app/manage.py app/vapormap/__init__.py app/vapormap/urls.py app/vapormap/wsgi.py app/vapormap/settings/__init__.py app/vapormap/settings/base.py app/vapormap/settings/development.py app/vapormap/settings/production.py app/api/__init__.py app/api/api_test.py app/api/apps.py app/api/models.py app/api/tests.py app/api/views.py app/api/migrations/0001_initial.py app/api/migrations/0002_auto_20190524_1256.py app/api/migrations/0003_auto_20190529_1149.py app/api/migrations/__init__.py
Wrote HTML report to htmlcov/index.html
[Pipeline] sh
+ coverage xml --ignore-errors app/manage.py app/vapormap/__init__.py app/vapormap/urls.py app/vapormap/wsgi.py app/vapormap/settings/__init__.py app/vapormap/settings/base.py app/vapormap/settings/development.py app/vapormap/settings/production.py app/api/__init__.py app/api/api_test.py app/api/apps.py app/api/models.py app/api/tests.py app/api/views.py app/api/migrations/0001_initial.py app/api/migrations/0002_auto_20190524_1256.py app/api/migrations/0003_auto_20190529_1149.py app/api/migrations/__init__.py
Wrote XML report to coverage.xml
Post stage
[Pipeline] publishHTML
[htmlpublisher] Archiving HTML reports...
[htmlpublisher] Archiving at PROJECT level /var/jenkins_home/workspace/Jenkins-Project/htmlcov to /var/jenkins_home/jobs/Jenkins-Project/htmlreports/Code_20Coverage_20Report
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Build-image)
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/Jenkins-Project@2
[Pipeline] {
[Pipeline] checkout
The recommended git tool is: git
No credentials specified
Warning: JENKINS-30600: special launcher org.jenkinsci.plugins.docker.workflow.WithContainerStep$Decorator$1@52b50c73; decorates hudson.Launcher$LocalLauncher@12cfe16d will be ignored (a typical symptom is the Git executable not being run inside a designated container)
 > git rev-parse --resolve-git-dir /var/jenkins_home/workspace/Jenkins-Project@2/.git # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://gitlab.com/gbazack/jenkins-project # timeout=10
Fetching upstream changes from https://gitlab.com/gbazack/jenkins-project
 > git --version # timeout=10
 > git --version # 'git version 2.30.2'
 > git fetch --tags --force --progress -- https://gitlab.com/gbazack/jenkins-project +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision 9862df3d313ce90030d94eb3d86f5ab6afc8c54f (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9862df3d313ce90030d94eb3d86f5ab6afc8c54f # timeout=10
Commit message: "update: ci pipeline"
[Pipeline] withEnv
[Pipeline] {
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker inspect -f . docker:20.10
.
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] withDockerContainer
Jenkins seems to be running inside container 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker run -t -d -u 0:0 -w /var/jenkins_home/workspace/Jenkins-Project@2 --volumes-from 47c1b0e793c6e444b476a846a9d2e20dae706806a7e997f1b1185b3e841c8635 -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** -e ******** docker:20.10 cat
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker top 61a3474b29848e23f8b2a5d1cb46992a5aec92a7a0e56af54bcf979e4a62488e -eo pid,comm
ERROR: The container started but didn't run the expected command. Please double check your ENTRYPOINT does execute the command passed as docker run argument, as required by official docker images (see https://github.com/docker-library/official-images#consistency for entrypoint consistency requirements).
Alternatively you can force image entrypoint to be disabled by adding option `--entrypoint=''`.
[Pipeline] {
[Pipeline] echo
Building the docker image
[Pipeline] script
[Pipeline] {
[Pipeline] withDockerRegistry
[Pipeline] {
[Pipeline] sh
+ docker login -u gbazack -p glpat-zqwTcEci6-DyBCFFHA72 https://registry.gitlab.com
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker build -t registry.gitlab.com/gbazack/jenkins-project/backend:latest ./app
Sending build context to Docker daemon  47.62kB
Step 1/9 : FROM python:3.8 AS builder
 ---> 0797d0b771a5
Step 2/9 : WORKDIR /app
 ---> Using cache
 ---> 4ef20067127d
Step 3/9 : COPY . .
 ---> Using cache
 ---> 843d0513e9fe
Step 4/9 : RUN python -m pip install --upgrade pip
 ---> Using cache
 ---> 93ea8bfa9f6a
Step 5/9 : RUN pip install --no-cache-dir -r requirements/production.txt
 ---> Using cache
 ---> 8f1fd5647ae3
Step 6/9 : RUN pip install --no-cache-dir pytz PyMySQL
 ---> Using cache
 ---> 210eca358501
Step 7/9 : RUN python manage.py collectstatic --no-input
 ---> Using cache
 ---> 53b258945118
Step 8/9 : EXPOSE 8000
 ---> Using cache
 ---> 044411307d0d
Step 9/9 : CMD python manage.py makemigrations    && python manage.py migrate    && gunicorn vapormap.wsgi:application --bind 0.0.0.0:8000
 ---> Using cache
 ---> ad910f0390f0
Successfully built ad910f0390f0
Successfully tagged registry.gitlab.com/gbazack/jenkins-project/backend:latest
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker tag registry.gitlab.com/gbazack/jenkins-project/backend:latest registry.gitlab.com/gbazack/jenkins-project/backend:latest
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] isUnix
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
+ docker push registry.gitlab.com/gbazack/jenkins-project/backend:latest
The push refers to repository [registry.gitlab.com/gbazack/jenkins-project/backend]
54846f60b82c: Preparing
e26c81e7efc5: Preparing
49f06b83944f: Preparing
f262e903583d: Preparing
cbf5c5123ba7: Preparing
2c5b43315967: Preparing
9e224f499e0a: Preparing
ff1f2a3b1a2d: Preparing
5b9e7e39f904: Preparing
182f3ce3bf27: Preparing
316e3949bffa: Preparing
e3f84a8cee1f: Preparing
48144a6f44ae: Preparing
26d5108b2cba: Preparing
89fda00479fc: Preparing
2c5b43315967: Waiting
9e224f499e0a: Waiting
e3f84a8cee1f: Waiting
48144a6f44ae: Waiting
ff1f2a3b1a2d: Waiting
5b9e7e39f904: Waiting
26d5108b2cba: Waiting
89fda00479fc: Waiting
182f3ce3bf27: Waiting
316e3949bffa: Waiting
cbf5c5123ba7: Layer already exists
54846f60b82c: Layer already exists
f262e903583d: Layer already exists
49f06b83944f: Layer already exists
e26c81e7efc5: Layer already exists
9e224f499e0a: Layer already exists
ff1f2a3b1a2d: Layer already exists
182f3ce3bf27: Layer already exists
5b9e7e39f904: Layer already exists
2c5b43315967: Layer already exists
316e3949bffa: Layer already exists
e3f84a8cee1f: Layer already exists
48144a6f44ae: Layer already exists
26d5108b2cba: Layer already exists
89fda00479fc: Layer already exists
latest: digest: sha256:cba3ae2bebdea6275ca624eccf7f04d6a60a0034ffe1e6501878159b221110ee size: 3475
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // withDockerRegistry
[Pipeline] }
[Pipeline] // script
[Pipeline] }
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker stop --time=1 61a3474b29848e23f8b2a5d1cb46992a5aec92a7a0e56af54bcf979e4a62488e
$ docker exec --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** --env ******** fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d docker rm -f 61a3474b29848e23f8b2a5d1cb46992a5aec92a7a0e56af54bcf979e4a62488e
[Pipeline] // withDockerContainer
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
$ docker stop --time=1 fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d
$ docker rm -f fb2d0bb9b3333d04c844d207e0a5f838c6a101de9d6b683bda0490568f565d5d
[Pipeline] // withDockerContainer
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
