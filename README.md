# Description

1. **Application**:<br>

This project is a web application for training purposes developed by `Tristan Le Toullec`. With this web application, you can record the geographical coordinates (or GPS points) with a simple click or by manual entries. Then it displays this location on a map.<br>

The application's architecture has three main components: the frontend, backend, and database. Each part runs on different servers but communicates to provide the end-users with the services needed. The frontend is a static website developed with `HTML`, `CSS` and `Javascript` and can be deployed on an Nginx web server. Nginx will also serve as a reverse proxy to redirect the requests to the frontend and backend. The backend is developed in `Python` with the framework `Django`. The communication between the Nginx server and the backend is handled by `Gunicorn`. It is a Python Web Server Gateway Interface (WSGI) HTTP server that supports `Django` natively. The geographical coordinates are stored in a `SQL` database managed by a `MariaDB` server, an open-source `MySQL` relational database management system. The figure below illustrates the global architecture of the application.

<img src=img/vapormap-app.png width="350" height="400" title="Block diagram of the proposed architecture" style="margin-left: 300px;"/>


2. **Jenkins CI pipeline**:<br>
The pipepline for continous integration of this application is carried out by a Jenkins server.
The stages implemented in the jenkins pipeline (see the `Jenkinsfile`) are:
 - ***Checkout***: establish connection with Gitlab and clone the project. 
 - ***Init-env***: prepare the environment by creating files and directories that will be used later.
 - ***Security***: analyse and detect security threats from the Python codes
   - **[Bandit](https://bandit.readthedocs.io/en/latest/)**: is a tool designed to find common security issues in Python code. Bandit processes each file, builds an AST from it, and runs appropriate plugins against the AST nodes. Once Bandit has finished scanning all the files, it generates a report.
   - **[Safety](https://pypi.org/project/safety/)**: checks your installed dependencies for known security vulnerabilities. By default it uses the open Python vulnerability database Safety DB.
 - ***Codequality***: a measure of quality to assesses whether or not the code meets the requirements that caused it to be written.
   - **[Hadolint](https://hub.docker.com/r/hadolint/hadolint)**: is a linter tool that helps to validate a Dockerfile to build Docker images that follow the recommendations of the Best practices for writing Dockerfiles guide.
   - **[Flake8](https://pypi.org/project/flake8/)**: Flake8 is a wrapper around the tools like `PyFlakes`, `Pycodestyle` and `Ned Batchelder’s McCabe script`. Flake8 runs all the tools by launching the single flake8 command. It displays the warnings in a per-file, merged output.
   - **[Pylama]()**: is a code audit tool for Python. Pylama wraps the tools including `Pycodestyle`, `Pydocstyle`, `PyFlakes`, `Mccabe`, `Pylint`, `Radon`, `eradicate`,  `Mypy` and `Vulture`.
 - ***[Coverage](https://coverage.readthedocs.io/en/6.3.2/)***: is a tool for measuring code coverage of Python programs. It monitors your program, noting which parts of the code have been executed, then analyzes the source to identify code that could have been executed but was not.
 - ***Build***: a docker image and store in the gitlab container registry ([Docker image](https://gitlab.com/gbazack/jenkins-project/container_registry)).

<img src=img/jenkinspipeline.png width="90%" height="90%" title="Block diagram of the proposed architecture" style="margin-left: 30%;"/>


# Usage

1. **Preparing the environment**: <br>
Clone the repository to your local computer

```shell
URL=https://gitlab.com/gbazack/jenkins-project
git clone $URL jenkins-project
```
2. **Environment variables**:<br>
Some variables are used in the jenkins pipeline and others are required for deploying the application.
   - Variables in the Jenkins pipeline:
     - BACKEND_IMAGE_TAG: the image tag
     - REGISTRY_IMAGE: image name as suggested by gitlab
     - REGISTRY_USER: gitlab user
     - REGISTRY_TOKEN: gitlab API token
     - REGISTRY_URL: url of gitlab registry
   - Variables for deployment: can be found in the file `.env`.   

1. **Setting up Jenkins server**:<br>
First, you need to need a Jenkins server. You can deploy the jenkins server on a [virtual machine](https://www.jenkins.io/doc/book/installing/linux/, "Click here") or as a container. For the containerized option, the following commands enable to deploy a `Docker` container:

```shell
cd jenkins-project/
docker-compose -up -d
```

The Gitlab plugin must be installed and configured on the Jenkins server to authorize the connection to GitLab.To install and configure the plugin, visit this page: [Jenkins integration](https://docs.gitlab.com/ee/integration/jenkins.html).

4. **Additional Jenkins plugins**:<br>
Below is a list of plugins that the pipeline requires.
> - `Blue Ocean`: allows users to graphically create, visualize and diagnose Continuous Delivery (CD) Pipelines.
> - `Clover`: allows you to capture code coverage reports from OpenClover.
> - `Corbertura`: allows you to capture code coverage report from Cobertura.
> - `Code Coverage API Plugin`: integrates and publishes multiple coverage report types.
> - `OpenCover`: allows users to publish your OpenCover coverage reports into Jenkins.
> - `GitLab Authentication`: provides a means of securing a Jenkins instance by offloading authentication and authorization to GitLab
> - `GitLab Branch Source`: allows you to create job based on GitLab user or group or subgroup project(s).
> - `HTML Publisher`: is useful to publish HTML reports that your build generates to the job and build pages.
> - `Python`: Adds the ability to execute python scripts as build steps.
> - `Pyenv Pipeline`: wraps a block in a Python virtualenv. pysh or pybat steps are deprecated, and are simply copies or sh and bat respectively
> - `Quality Gates`: will fail the build whenever the Quality Gates criteria in the Sonar analysis aren't met.
> - `Warning Next Generation`: collects compiler warnings or issues reported by static analysis tools and visualizes the results.

# Deploy
Make sure you update the environment variables in `.env` file. Then you can deploy the application with the commands below.
```shell
cd jenkins-project/app/
docker-compose --env-file .env up -d
```

If your developement environment is on your local computer, test the backend with command:
```shell
curl http://localhost:8000/api/?format=json
```

Expected output:
```shell
{"points":"http://localhost:8000/api/points/?format=json"}
```
